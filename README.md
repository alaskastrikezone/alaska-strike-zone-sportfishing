Looking for the best Ketchikan fishing charters? Fish for Alaskan salmon and halibut on our half-day and full-day fishing excursions. Fish world-class Alaskan waters aboard 28-foot charter boats using our top-of-the-line equipment. Ketchikan fishing is world-class fishing. Shuttle service available.

Address: 407 Knudson Cove Road, Ketchikan, AK 99901, USA

Phone: 907-247-5663

